FROM alpine:3.8

ENTRYPOINT ["/pipe.sh"]

CMD ["java","-Dspring.profiles.active=${SPRING_PROFILE}","${MAIN_CLASS}""]